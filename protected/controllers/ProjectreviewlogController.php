<?php

class ProjectreviewlogController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Projectreviewlog'),
		));
	}

	public function actionCreate() {
		$model = new Projectreviewlog;


		if (isset($_POST['Projectreviewlog'])) {
			$model->setAttributes($_POST['Projectreviewlog']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Projectreviewlog');


		if (isset($_POST['Projectreviewlog'])) {
			$model->setAttributes($_POST['Projectreviewlog']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Projectreviewlog')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Projectreviewlog');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Projectreviewlog('search');
		$model->unsetAttributes();

		if (isset($_GET['Projectreviewlog']))
			$model->setAttributes($_GET['Projectreviewlog']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}