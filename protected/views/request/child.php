    <h1><?php echo Yii::t('app', 'Temas por Solicitud'); ?></h1>

        <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                        array('label' => 'Estudiante', 
                            'value'=> $model->student->studentbatchid . ' - ' . $model->student->studentname 
                        ,),
                        array('label'=>'Comentarios',
                            'value'=> $model->comment,),
                ),
        )); ?>
        <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'child-grid',
                        'dataProvider'=>$child_model->searchByParent($parentId),
                        'columns'=>array(
                            //array('name'=>'id','htmlOptions'=>array('width'=>'60'),),
                            array(
				'name'=>'topicid',
				'value'=>'GxHtml::valueEx($data->topic)',
				'filter'=>GxHtml::listDataEx(Topic::model()->findAllAttributes(null, true)),
				),
                            array(
                                    'class'=>'EJuiDlgsColumn',
                                     'template'=>'{delete}',
                                     'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                                        'buttons'=>array(
                                                 'delete' => array(
                                                    'label'=> 'Eliminar',
                                                     'url'=>'Yii::app()->createUrl("topic/delete", array("id"=>$data->primaryKey))',
                                                 ),

                                    ),
                                ),
                            ),
                    ));
            
        ?>
<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'topic/create/'.$parentId,
        'dialogTitle' => 'Agregar ',
        'dialogWidth' => 600,
        'dialogHeight' => 400,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'child-grid' //the grid with this id will be refreshed after closing
    )
);
?>
<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'topic/select/'.$parentId,
        'dialogTitle' => 'Seleccionar',
        'dialogWidth' => 600,
        'dialogHeight' => 400,
        'openButtonText' => 'Seleccionar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'child-grid' //the grid with this id will be refreshed after closing
    )
);
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('request/admin'))); ?>
<!-- http://www.yiiframework.com/forum/index.php/topic/32985-extension-quickdlgs/ -->