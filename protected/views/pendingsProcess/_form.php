<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'pendings-process-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 28)); ?>
		<?php echo $form->error($model,'id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'studentid'); ?>
		<?php echo $form->textField($model, 'studentid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'studentid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'studentname'); ?>
		<?php echo $form->textField($model, 'studentname', array('maxlength' => 256)); ?>
		<?php echo $form->error($model,'studentname'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'studentbatchid'); ?>
		<?php echo $form->textField($model, 'studentbatchid', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'studentbatchid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'entrydate'); ?>
		<?php echo $form->textField($model, 'entrydate'); ?>
		<?php echo $form->error($model,'entrydate'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'statusid'); ?>
		<?php echo $form->textField($model, 'statusid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'statusid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'statusdescription'); ?>
		<?php echo $form->textField($model, 'statusdescription', array('maxlength' => 256)); ?>
		<?php echo $form->error($model,'statusdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'flowdoctoid'); ?>
		<?php echo $form->textField($model, 'flowdoctoid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'flowdoctoid'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->