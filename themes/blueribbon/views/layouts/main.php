<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.js" type="text/javascript"></script>
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
    
<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->
	<div id="mainmenu">
		<?php 
                    $rolid=Yii::app()->user->GetState('rolid');   
                    
                    $menudef=MenuBuild::getMenuTree($rolid);
                    $menu=array(
                                'id'=> 'jsddm',
                                'linkLabelWrapper'=>null,
                                'htmlOptions'=>array('class'=>'topNav'),
                                'items'=>$menudef                            
                               );
                   $this->widget('ext.widgets.ddmenu.XDropDownMenu', array(
                            'items'=>$menudef,
                        ));
                ?>
	</div><!-- mainmenu -->
        <p></p>        
	<?php /*$this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
            
	));*/ ?><!-- breadcrumbs -->

	<?php echo $content; ?>

	<div id="footer">
            Copyright &copy; <?php echo date('Y'); ?> <br/>Victor Steve López Camey.<br/>
		Todos los derechos reservados<br/>
		<?php /*echo Yii::powered();*/ ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>