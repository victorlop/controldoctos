<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('shortdescription')); ?>:
	<?php echo GxHtml::encode($data->shortdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('longdescription')); ?>:
	<?php echo GxHtml::encode($data->longdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estimateddate')); ?>:
	<?php echo GxHtml::encode($data->estimateddate); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('projectid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->project)); ?>
	<br />

</div>