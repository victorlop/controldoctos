<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'status-form',
	'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
            <?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
            <?php echo $form->hiddenField($model, 'sort',array('value'=>1)); ?>
        <?php echo $form->hiddenField($model, 'endstatus',array('value'=>0)); ?>
		<div class="row">
		<?php echo $form->labelEx($model,'statusdescription'); ?>
		<?php echo $form->textField($model, 'statusdescription', array('maxlength' => 256,'size'=>50)); ?>
		<?php echo $form->error($model,'statusdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'amountreviews'); ?>
		<?php echo $form->textField($model, 'amountreviews',array('maxlength'=>1,'size'=>5)); ?>
		<?php echo $form->error($model,'amountreviews'); ?>
		</div><!-- row -->
		
		<div class="row">
		<?php echo $form->labelEx($model,'maxdays'); ?>
		<?php echo $form->textField($model, 'maxdays',array('maxlength'=>3,'size'=>10)); ?>
		<?php echo $form->error($model,'maxdays'); ?>
		</div><!-- row -->
                
                <div class="row">
		<?php echo $form->labelEx($model,'allowinactivation'); ?>
		<?php echo $form->checkBox($model, 'allowinactivation'); ?>
		<?php echo $form->error($model,'allowinactivation'); ?>
		</div><!-- row -->

                <div class="row">
		<?php /*echo $form->labelEx($model,'endstatus'); */?>
                <?php /*echo $form->dropDownList($model, 'endstatus',array('No','Si'), array('prompt' => Yii::t('app', '(seleccionar)'))); */?>
		<?php /*echo $form->error($model,'endstatus'); */?>
		</div><!-- row -->
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('flowdoctoDetails'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'flowdoctoDetails', GxHtml::encodeEx(GxHtml::listDataEx(FlowdoctoDetail::model()->findAllAttributes(null, true)), false, true));*/ ?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('preprojects'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'preprojects', GxHtml::encodeEx(GxHtml::listDataEx(Preproject::model()->findAllAttributes(null, true)), false, true));*/ ?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('preprojectreviews')); */?></label>
		<?php /*echo $form->checkBoxList($model, 'preprojectreviews', GxHtml::encodeEx(GxHtml::listDataEx(Preprojectreview::model()->findAllAttributes(null, true)), false, true)); */?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('projects'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'projects', GxHtml::encodeEx(GxHtml::listDataEx(Project::model()->findAllAttributes(null, true)), false, true)); */?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('projectreviews'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'projectreviews', GxHtml::encodeEx(GxHtml::listDataEx(Projectreview::model()->findAllAttributes(null, true)), false, true)); */?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('rolstatuses'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'rolstatuses', GxHtml::encodeEx(GxHtml::listDataEx(Rolstatus::model()->findAllAttributes(null, true)), false, true));*/ ?>

<?php
echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->