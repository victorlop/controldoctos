<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('request-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Solicitudes de Tema'); ?></h1>

<p>
Opcionalmente, puede ingresar un operador (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al inicio de cada valor de búsqueda para especificar criterios personalizados.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Búsqueda avanzada'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display: none">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'request-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'45'),),
                array(
				'name'=>'studentid',
				'value'=>'GxHtml::valueEx($data->student)',
                                'htmlOptions'=>array('width'=>'150'),
				'filter'=>GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1))),
				),
                array('name'=>'entrydate','filter'=>false,'htmlOptions'=>array('width'=>'120'),),
		'comment',
		array(
				'name'=>'statusid',
                                'filter'=>false,
                                'htmlOptions'=>array('width'=>'100'),
				'value'=>'GxHtml::valueEx($data->status)',
				),
		array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{view}{update}{delete}{detail}',
                    'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                    //'viewButtonImageUrl'=>Yii::app()->baseUrl .'images/dialogview.png',
                    
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),
                                'update' => array(
                                    'label'=> 'Editar',
                                 ),
                                'delete' => array(
                                   'label'=> 'Eliminar',
                                ),
                                'detail' => array(
                                    'label' => 'Temas',
                                    'imageUrl'=>Yii::app()->baseUrl .'/images/topic.png',
                                    'url'=>'Yii::app()->createUrl("request/child", array("id"=>$data->primaryKey))',
                                ),/*
                                'status'=>array(
                                    'label' => 'Estados donde participa',
                                    'imageUrl'=>Yii::app()->baseUrl .'/images/rol_status.png',
                                    'url'=>'Yii::app()->createUrl("rol/status", array("id"=>$data->primaryKey))',
                                ),*/
                                
                   ),
                   'viewDialog'=>array(
                       'controllerRoute' => 'view', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Detalle - Solicitud',
                       'hideTitleBar' => true, 
                       'dialogWidth' => 600, //use the value from the dialog config
                       'dialogHeight' => 400,
                       'closeButtonText' => 'Cerrar'
                   ),

               //the attributes for the EFrameJuiDlg widget. use like the 'attributes' param from EQuickDlgs::iframeButton
                   'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Solicitud',
                       'dialogWidth' => 600, //override the value from the dialog config
                       'dialogHeight' => 400,
                       'closeButtonText' => 'Cerrar'
                   ), 
               ),
	),
)); ?>
<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'create',
        'dialogTitle' => 'Agregar - Solicitud',
        'dialogWidth' => 600,
        'dialogHeight' => 400,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'request-grid' //the grid with this id will be refreshed after closing
    )
);

?>