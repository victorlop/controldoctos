<?php

class RequestController extends GxController {


	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Request')));
	}

	public function actionCreate() {
		$model = new Request;
                $this->performAjaxValidation($model);
                
		if (isset($_POST['Request'])) {
			$model->setAttributes($_POST['Request']);
                        $model->active=1;
                        
			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Request');
                $this->performAjaxValidation($model);
    
		if (isset($_POST['Request'])) {
			$model->setAttributes($_POST['Request']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            $row=  $this->loadModel($id,'Request');

            if (!$row->status->allowinactivation)
                throw new CHttpException(404, 'Estado invalido para operacion.');
                
            $row->active=0;
            $row->save();
            
            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Request('search');
		$model->unsetAttributes();

		if (isset($_GET['Request']))
			$model->setAttributes($_GET['Request']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Request('search');
		$model->unsetAttributes();

		if (isset($_GET['Request']))
			$model->setAttributes($_GET['Request']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        public function actionChild($id) {
                 $child_model = new Requestdetail("searchByParent");
                 $child_model->unsetAttributes();
                 EQuickDlgs::render('child',array(
                                    'model'=>$this->loadModel($id,'Request'),
                                    'child_model'=>$child_model,
                                    'parentId' => $id));
        }
}