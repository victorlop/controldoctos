<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('dateentry')); ?>:
	<?php echo GxHtml::encode($data->dateentry); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('dateend')); ?>:
	<?php echo GxHtml::encode($data->dateend); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('teacherid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->teacher)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('projectid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->project)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('statusid')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->status)); ?>
	<br />

</div>