<?php

class StudentController extends GxController {


	public function actionView($id) {
            EQuickDlgs::render('view',array('model'=>$this->loadModel($id, 'Student')));
	}

	public function actionCreate() {
		$model = new Student;
                $this->performAjaxValidation($model);               

		if (isset($_POST['Student'])) {
			$model->setAttributes($_POST['Student']);
                        $model->status=1;

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Student');
                $this->performAjaxValidation($model);

		if (isset($_POST['Student'])) {
			$model->setAttributes($_POST['Student']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            $row=  $this->loadModel($id,'Student');
            $row->status=0;
            $row->save();

            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            
	}

	public function actionIndex() {
		$model = new Student('search');
		$model->unsetAttributes();

		if (isset($_GET['Student']))
			$model->setAttributes($_GET['Student']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Student('search');
		$model->unsetAttributes();

		if (isset($_GET['Student']))
			$model->setAttributes($_GET['Student']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}