<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'preproject-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'preprojectdescription'); ?>
		<?php echo $form->textField($model, 'preprojectdescription', array('maxlength' => 1024)); ?>
		<?php echo $form->error($model,'preprojectdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments'); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
		<?php echo $form->error($model,'dateentry'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'studentid'); ?>
		<?php echo $form->textField($model, 'studentid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'studentid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'statusid'); ?>
		<?php echo $form->textField($model, 'statusid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'statusid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model, 'active'); ?>
		<?php echo $form->error($model,'active'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'requestdetailid'); ?>
		<?php echo $form->dropDownList($model, 'requestdetailid', GxHtml::listDataEx(Requestdetail::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'requestdetailid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'flowdoctoid'); ?>
		<?php echo $form->textField($model, 'flowdoctoid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'flowdoctoid'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('preprojectreviews')); ?></label>
		<?php echo $form->checkBoxList($model, 'preprojectreviews', GxHtml::encodeEx(GxHtml::listDataEx(Preprojectreview::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->