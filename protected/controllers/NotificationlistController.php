<?php

class NotificationlistController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Notificationlist'),
		));
	}

	public function actionCreate() {
		$model = new Notificationlist;


		if (isset($_POST['Notificationlist'])) {
			$model->setAttributes($_POST['Notificationlist']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Notificationlist');


		if (isset($_POST['Notificationlist'])) {
			$model->setAttributes($_POST['Notificationlist']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Notificationlist')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Notificationlist');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Notificationlist('search');
		$model->unsetAttributes();

		if (isset($_GET['Notificationlist']))
			$model->setAttributes($_GET['Notificationlist']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}