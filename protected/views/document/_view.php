<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('documentname')); ?>:
	<?php echo GxHtml::encode($data->documentname); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('coursebyteacherid')); ?>:
	<?php echo GxHtml::encode($data->coursebyteacherid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('preprojectid')); ?>:
	<?php echo GxHtml::encode($data->preprojectid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('projectid')); ?>:
	<?php echo GxHtml::encode($data->projectid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('reviewlogid')); ?>:
	<?php echo GxHtml::encode($data->reviewlogid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('requestid')); ?>:
	<?php echo GxHtml::encode($data->requestid); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('projectdetailid')); ?>:
	<?php echo GxHtml::encode($data->projectdetailid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('support')); ?>:
	<?php echo GxHtml::encode($data->support); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('documentdescription')); ?>:
	<?php echo GxHtml::encode($data->documentdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('filename')); ?>:
	<?php echo GxHtml::encode($data->filename); ?>
	<br />
	*/ ?>

</div>