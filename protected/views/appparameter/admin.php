<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('appparameter-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Parametros'); ?></h1>

<p>
Opcionalmente, puede ingresar un operador (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al inicio de cada valor de búsqueda para especificar criterios personalizados.
</p>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'appparameter-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
        'filterPosition'=>'none',
	'columns' => array(
		'id',
		'code',
		'parameterdescription',
		//'parametervalue',   
                array(
                    'name'=>'Valor',
                    'value'=> function($data,$row) {
                                if ($data->code == 'REQ' or $data->code == 'ANT' or $data->code == 'PRJ') {
                                    $valor = (int)$data->parametervalue;
                                    $Flow = Flowdocto::model()->findByPk($valor);

                                    if ($Flow)
                                        return $Flow->flowdescription;
                                    else
                                        return $data->parametervalue;
                                } else {
                                    return $data->parametervalue;
                                }
                            } ,
                ),
		array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{update}',
                    'updateDialog'=>array(
                        'controllerRoute' => 'update', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Modificar - Parámetros',
                        'dialogWidth' => 600, //override the value from the dialog config
                        'dialogHeight' => 400,
                        'closeButtonText' => 'Cerrar'
                    ), 
               ),
	),
)); ?>