<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'dateend'); ?>
		<?php echo $form->textField($model, 'dateend'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'teacherid'); ?>
		<?php echo $form->dropDownList($model, 'teacherid', GxHtml::listDataEx(Teacher::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'projectid'); ?>
		<?php echo $form->dropDownList($model, 'projectid', GxHtml::listDataEx(Project::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'statusid'); ?>
		<?php echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
