<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'requestdetail-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'requestid'); ?>
		<?php echo $form->dropDownList($model, 'requestid', GxHtml::listDataEx(Request::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'requestid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'topicid'); ?>
		<?php echo $form->dropDownList($model, 'topicid', GxHtml::listDataEx(Topic::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'topicid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'approved'); ?>
		<?php echo $form->textField($model, 'approved'); ?>
		<?php echo $form->error($model,'approved'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('preprojects')); ?></label>
		<?php echo $form->checkBoxList($model, 'preprojects', GxHtml::encodeEx(GxHtml::listDataEx(Preproject::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->