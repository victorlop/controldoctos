<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'teacher-form',
	'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>

		<div class="row">
		<?php echo $form->labelEx($model,'teachername'); ?>
		<?php echo $form->textField($model, 'teachername', array('maxlength' => 1024,'size'=> 50)); ?>
		<?php echo $form->error($model,'teachername'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 1024,'size'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div><!-- row -->                
		<div class="row">
		<?php echo $form->labelEx($model,'canreview'); ?>
		<?php echo $form->dropDownList($model, 'canreview',array('No','Si'), array('prompt' => Yii::t('app', '(seleccionar)'))); ?>
		<?php echo $form->error($model,'canreview'); ?>
		</div><!-- row -->

		<label><?php /*echo GxHtml::encode($model->getRelationLabel('coursebyteachers'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'coursebyteachers', GxHtml::encodeEx(GxHtml::listDataEx(Coursebyteacher::model()->findAllAttributes(null, true)), false, true));*/ ?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('preprojectreviews'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'preprojectreviews', GxHtml::encodeEx(GxHtml::listDataEx(Preprojectreview::model()->findAllAttributes(null, true)), false, true));*/ ?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('projectreviews'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'projectreviews', GxHtml::encodeEx(GxHtml::listDataEx(Projectreview::model()->findAllAttributes(null, true)), false, true)); */?>

<?php
echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->