<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'preprojectdescription'); ?>
		<?php echo $form->textField($model, 'preprojectdescription', array('maxlength' => 1024)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'comments'); ?>
		<?php echo $form->textArea($model, 'comments'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'studentid'); ?>
		<?php echo $form->textField($model, 'studentid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'statusid'); ?>
		<?php echo $form->textField($model, 'statusid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'active'); ?>
		<?php echo $form->textField($model, 'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'requestdetailid'); ?>
		<?php echo $form->dropDownList($model, 'requestdetailid', GxHtml::listDataEx(Requestdetail::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'flowdoctoid'); ?>
		<?php echo $form->textField($model, 'flowdoctoid', array('maxlength' => 20)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
