<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'career-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
		<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model, 'code', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'code'); ?>
		</div><!-- row -->
                
		<div class="row">
		<?php echo $form->labelEx($model,'careerdescription'); ?>
		<?php echo $form->textField($model, 'careerdescription', array('maxlength' => 256)); ?>
		<?php echo $form->error($model,'careerdescription'); ?>
		</div><!-- row -->

                		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments',
                                            array('cols'=>50, 'rows'=>5)); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->

<?php
echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->