<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'document-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'documentname'); ?>
		<?php echo $form->textArea($model, 'documentname'); ?>
		<?php echo $form->error($model,'documentname'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'coursebyteacherid'); ?>
		<?php echo $form->textField($model, 'coursebyteacherid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'coursebyteacherid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'preprojectid'); ?>
		<?php echo $form->textField($model, 'preprojectid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'preprojectid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'projectid'); ?>
		<?php echo $form->textField($model, 'projectid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'projectid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'reviewlogid'); ?>
		<?php echo $form->textField($model, 'reviewlogid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'reviewlogid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'requestid'); ?>
		<?php echo $form->textField($model, 'requestid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'requestid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'projectdetailid'); ?>
		<?php echo $form->textField($model, 'projectdetailid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'projectdetailid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'support'); ?>
		<?php echo $form->textField($model, 'support'); ?>
		<?php echo $form->error($model,'support'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'documentdescription'); ?>
		<?php echo $form->textArea($model, 'documentdescription'); ?>
		<?php echo $form->error($model,'documentdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'filename'); ?>
		<?php echo $form->textArea($model, 'filename'); ?>
		<?php echo $form->error($model,'filename'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->