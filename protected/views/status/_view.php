<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('statusdescription')); ?>:
	<?php echo GxHtml::encode($data->statusdescription); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sort')); ?>:
	<?php echo GxHtml::encode($data->sort); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('amountreviews')); ?>:
	<?php echo GxHtml::encode($data->amountreviews); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('endstatus')); ?>:
	<?php echo GxHtml::encode($data->endstatus); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('maxdays')); ?>:
	<?php echo GxHtml::encode($data->maxdays); ?>
	<br />

</div>