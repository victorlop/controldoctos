<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'shortdescription'); ?>
		<?php echo $form->textField($model, 'shortdescription', array('maxlength' => 1024)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'longdescription'); ?>
		<?php echo $form->textArea($model, 'longdescription'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'estimateddate'); ?>
		<?php echo $form->textField($model, 'estimateddate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'projectid'); ?>
		<?php echo $form->dropDownList($model, 'projectid', GxHtml::listDataEx(Project::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
