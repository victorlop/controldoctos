<?php

class PreprojectreviewController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Preprojectreview'),
		));
	}

	public function actionCreate() {
		$model = new Preprojectreview;


		if (isset($_POST['Preprojectreview'])) {
			$model->setAttributes($_POST['Preprojectreview']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Preprojectreview');


		if (isset($_POST['Preprojectreview'])) {
			$model->setAttributes($_POST['Preprojectreview']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Preprojectreview')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Preprojectreview');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Preprojectreview('search');
		$model->unsetAttributes();

		if (isset($_GET['Preprojectreview']))
			$model->setAttributes($_GET['Preprojectreview']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}