<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'statusdescription'); ?>
		<?php echo $form->textField($model, 'statusdescription', array('maxlength' => 256,'size'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'amountreviews'); ?>
		<?php echo $form->textField($model, 'amountreviews',array('maxlength'=>1,'size'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'maxdays'); ?>
		<?php echo $form->textField($model, 'maxdays',array('maxlength'=>3,'size'=>10)); ?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model, 'endstatus'); */?>
                <?php /*echo $form->dropDownList($model, 'endstatus',array('No','Si'), array('prompt' => Yii::t('app', 'Todos'))); */?>
	</div>
    
	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
