<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Control de Documentos',
        //'defaultController'=>'layouts/main',
        'language' => 'es',

	// preloading 'log' component
	'preload'=>array('log'),
	'theme'=>'shadow_dancer',

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'ext.quickdlgs.*',
                'ext.giix-components.*', // giix components
        	'ext.fileimagebehavior.*',
                'ext.widgets.portlet.XPortlet',
                'application.widgets.bootstrap.*',
                'ext.widgets.*'
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'controldoctosUMG',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('127.0.0.1','::1'),// If removed, Gii defaults to localhost only.
                        'generatorPaths' => array(
                            'ext.giix-core', // giix generators
                        ),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>FALSE,
		),
                'request'=>array(
                    'enableCsrfValidation'=>true,
                ),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
                        //'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
                'widgetFactory'=>array(
                    'widgets'=>array(
                        'CGridView'=>array(
                            'htmlOptions'=>array('cellspacing'=>'0','cellpadding'=>'0','width'=>'100%'),
					'itemsCssClass'=>'item-class',
					'pagerCssClass'=>'pager-class'
                        ),
                        'CJuiButton'=>array(
                            'htmlOptions'=>array('class'=>'shadowbutton'),
                        ),
                        /*'CJuiAccordion'=>array(
                            'htmlOptions'=>array('class'=>'shadowaccordion'),
                            //'theme'=>'shadow_dancer',
                        ),*/
                        'EFrameJuiDlg'=>array(
                            'dialogAttributes'=>array('theme'=>'shadow_dancer'),
                        ),
                        'EContentJuiDlg'=>array(
                            'dialogAttributes'=>array('theme'=>'shadow_dancer'),
                        ),
                        'EAjaxJuiDlg'=>array(
                            'dialogAttributes'=>array('theme'=>'shadow_dancer'),
                        ),
                        'CDetailView'=>array(
                            'htmlOptions'=>array('itemsCssClass'=>'item-class'),
					
                        ),
                        
                    ),
                ),
		
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=ControlDoctos',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);