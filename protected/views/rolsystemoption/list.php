<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'rolsystemoption-grid',
	'dataProvider' => $model->searchByParent(),
	
	'columns' => array(
		array('name'=>'id','htmlOptions'=>array('width'=>'60'),),
		array(
				'name'=>'systemoptionid',
				'value'=>'GxHtml::valueEx($data->systemoption)',
				'filter'=>GxHtml::listDataEx(Systemoption::model()->findAllAttributes(null, true)),
				),
		array(
			'class' => 'CButtonColumn',
		),
	),
));
?>