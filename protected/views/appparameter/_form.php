<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'appparameter-form',
	'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
            <?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
                <?php echo $form->hiddenField($model,'id'); ?>
		<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model, 'code', array('maxlength' => 10,'disabled'=>TRUE)); ?>
		<?php echo $form->error($model,'code'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'parameterdescription'); ?>
		<?php echo $form->textField($model, 'parameterdescription', array('maxlength' => 1024,'disabled'=>TRUE,'size'=>50)); ?>
		<?php echo $form->error($model,'parameterdescription'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'parametervalue'); ?>
		<?php 
                
                    if ($model->code == 'REQ' or $model->code == 'ANT' or $model->code == 'PRJ') {
                        echo $form->dropDownList($model, 'parametervalue', GxHtml::listDataEx(Flowdocto::model()->findAllAttributes(null, true, 'status=:active', array(':active' => 1))),array('prompt' => Yii::t('app', '(seleccionar)')));
                    } 
                    else {
                        echo $form->textField($model, 'parametervalue', array('maxlength' => 50,'size'=>5,'textalign'=>'right'));?><span> días</span> 
                    <?php } ?>
		<?php echo $form->error($model,'parametervalue'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->