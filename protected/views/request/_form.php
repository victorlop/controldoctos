<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'request-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
                <?php 
                
                    $flow = Appparameter::model()->find('code =:req',array(':req'=>'REQ'));
                    $statusid=0;
                    
                    if ($flow)
                    {
                        $valpar=(int)$flow->parametervalue;
                        $criteria= new CDbCriteria(array('order'=>'sort ASC', 'limit'=>1));
                        $criteria->condition = 'flowid=:flwid';
                        $criteria->params=array(':flwid'=>$valpar);
                        
                        $flowdocto= FlowdoctoDetail::model()->find($criteria);
                        if ($flowdocto)
                            $statusid = (int)$flowdocto->currentstatusid;
                    }
                        
                    /*echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All')));*/ 
                    echo $form->hiddenField($model,'statusid',array('value'=>$statusid));
                    echo $form->hiddenField($model,'flowdoctoid',array('value'=>$valpar));
                
                ?>
		<div class="row">
		<?php echo $form->labelEx($model,'studentid'); ?>
		<?php echo $form->dropDownList($model, 'studentid', GxHtml::listDataEx(Student::model()->findAllAttributes(null, true,'status=:status',array(':status'=>1), array('prompt' => Yii::t('app', 'Todos'))))); ?>
		<?php echo $form->error($model,'studentid'); ?>
		</div><!-- row -->
                <?php
                if (!$model->isNewRecord)
                { ?>
                    <div class="row">
                    <?php echo $form->labelEx($model,'entrydate'); ?>
                    <?php echo $form->textField($model, 'entrydate',array('disabled'=>TRUE)); ?>
                    <?php echo $form->error($model,'entrydate'); ?>
                    </div><!-- row -->
                <?php 
                }
                else
                    echo $form->hiddenField($model,'entrydate');
                ?>
		<div class="row">
		<?php echo $form->labelEx($model,'comment'); ?>
		<?php echo $form->textArea($model, 'comment',
                                            array('cols'=>50, 'rows'=>5)); ?>
		<?php echo $form->error($model,'comment'); ?>
		</div><!-- row -->

		<label><?php /*echo GxHtml::encode($model->getRelationLabel('requestdetails'));  */ ?></label>
		<?php /*echo $form->checkBoxList($model, 'requestdetails', GxHtml::encodeEx(GxHtml::listDataEx(Requestdetail::model()->findAllAttributes(null, true)), false, true)); */?>
		<label><?php /*echo GxHtml::encode($model->getRelationLabel('requestreviewlogs')); */ ?></label>
		<?php /*echo $form->checkBoxList($model, 'requestreviewlogs', GxHtml::encodeEx(GxHtml::listDataEx(Requestreviewlog::model()->findAllAttributes(null, true)), false, true));*/ ?>

<?php
echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->
<?php 
/*
$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'requestdetail-grid',
	'dataProvider' => $model->requestdetails,
	//'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'topicid',
				'value'=>'GxHtml::valueEx($data->topic)',
				'filter'=>GxHtml::listDataEx(Topic::model()->findAllAttributes(null, true)),
				),
		'approved',
		array(
			'class' => 'CButtonColumn',
		),
	),
));*/