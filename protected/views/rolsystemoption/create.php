<h1><?php echo Yii::t('app', 'Agregar Opción') ?></h1>

<?php
$this->renderPartial('_form', array(
		'model' => $model,
		'buttons' => 'create'));
?>