<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('teacher-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Catedráticos'); ?></h1>
<p>
Opcionalmente, puede ingresar un operador (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al inicio de cada valor de búsqueda para especificar criterios personalizados.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Búsqueda avanzada'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none;">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'teacher-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
                array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'60'),),
                array('name'=>'teachername','htmlOptions'=>array('width'=>'450'),),
		array(
                   'name'=>'canreview',
                   'header'=>'Revisor',
                   'filter'=>array('1'=>'Si','0'=>'No'),
                   'value'=>'($data->canreview=="1")?("Si"):("No")'
                ),
		array(
                   'class'=>'EJuiDlgsColumn',
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),

                                'delete' => array(
                                   'label'=> 'Eliminar',
                                ),
                                
                    ),
                    'viewDialog'=>array(
                        'controllerRoute' => 'view', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Detalle - Catedrático',
                        'hideTitleBar' => true, 
                        'dialogWidth' => 600, //use the value from the dialog config
                        'dialogHeight' => 400,
                        'closeButtonText' => 'Cerrar'
                    ),
                    'updateDialog'=>array(
                        'controllerRoute' => 'update', //=default
                        'actionParams' => array('id' => '$data->primaryKey'), //=default
                        'dialogTitle' => 'Modificar - Catedrático',
                        'dialogWidth' => 600, //override the value from the dialog config
                        'dialogHeight' => 400,
                        'closeButtonText' => 'Cerrar'
                    ),            
               ),
	),
)); ?>
<?php
EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'create',
        'dialogTitle' => 'Agregar - Catedrático',
        'dialogWidth' => 600,
        'dialogHeight' => 400,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'teacher-grid' //the grid with this id will be refreshed after closing
    )
);
?>