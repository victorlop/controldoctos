<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'projectreview-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
		<?php echo $form->error($model,'dateentry'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dateend'); ?>
		<?php echo $form->textField($model, 'dateend'); ?>
		<?php echo $form->error($model,'dateend'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'teacherid'); ?>
		<?php echo $form->dropDownList($model, 'teacherid', GxHtml::listDataEx(Teacher::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'teacherid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'projectid'); ?>
		<?php echo $form->dropDownList($model, 'projectid', GxHtml::listDataEx(Project::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'projectid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'statusid'); ?>
		<?php echo $form->dropDownList($model, 'statusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'statusid'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('projectreviewlogs')); ?></label>
		<?php echo $form->checkBoxList($model, 'projectreviewlogs', GxHtml::encodeEx(GxHtml::listDataEx(Projectreviewlog::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->