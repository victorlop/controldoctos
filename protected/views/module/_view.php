<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('descripcionmodule')); ?>:
	<?php echo GxHtml::encode($data->descripcionmodule); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('controller')); ?>:
	<?php echo GxHtml::encode($data->controller); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('defaultpage')); ?>:
	<?php echo GxHtml::encode($data->defaultpage); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('title')); ?>:
	<?php echo GxHtml::encode($data->title); ?>
	<br />

</div>