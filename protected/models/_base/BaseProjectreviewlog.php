<?php

/**
 * This is the model base class for the table "projectreviewlog".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Projectreviewlog".
 *
 * Columns in table "projectreviewlog" available as properties of the model,
 * followed by relations of table "projectreviewlog" available as properties of the model.
 *
 * @property string $id
 * @property string $dateentry
 * @property string $comments
 * @property string $projectreviewid
 * @property integer $status
 *
 * @property Projectreview $projectreview
 */
abstract class BaseProjectreviewlog extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'projectreviewlog';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Projectreviewlog|Projectreviewlogs', $n);
	}

	public static function representingColumn() {
		return 'dateentry';
	}

	public function rules() {
		return array(
			array('dateentry, projectreviewid, status', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('projectreviewid', 'length', 'max'=>20),
			array('comments', 'safe'),
			array('comments', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, dateentry, comments, projectreviewid, status', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'projectreview' => array(self::BELONGS_TO, 'Projectreview', 'projectreviewid'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'dateentry' => Yii::t('app', 'Dateentry'),
			'comments' => Yii::t('app', 'Comments'),
			'projectreviewid' => null,
			'status' => Yii::t('app', 'Status'),
			'projectreview' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('dateentry', $this->dateentry, true);
		$criteria->compare('comments', $this->comments, true);
		$criteria->compare('projectreviewid', $this->projectreviewid);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}