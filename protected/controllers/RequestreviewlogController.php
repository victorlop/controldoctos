<?php

class RequestreviewlogController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Requestreviewlog'),
		));
	}

	public function actionCreate() {
		$model = new Requestreviewlog;


		if (isset($_POST['Requestreviewlog'])) {
			$model->setAttributes($_POST['Requestreviewlog']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Requestreviewlog');


		if (isset($_POST['Requestreviewlog'])) {
			$model->setAttributes($_POST['Requestreviewlog']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Requestreviewlog')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Requestreviewlog');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Requestreviewlog('search');
		$model->unsetAttributes();

		if (isset($_GET['Requestreviewlog']))
			$model->setAttributes($_GET['Requestreviewlog']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}