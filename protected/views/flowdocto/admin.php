<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('flowdocto-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Flujos de Trabajo'); ?></h1>

<p>
Opcionalmente, puede ingresar un operador (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
o <b>=</b>) al inicio de cada valor de búsqueda para especificar criterios personalizados.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Búsqueda avanzada'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none;">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'flowdocto-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array('name'=>'id','filter'=>false,'htmlOptions'=>array('width'=>'60'),),
		'flowdescription',
		'comments',
		array(
                   'class'=>'EJuiDlgsColumn',
                    'template'=>'{view}{update}{delete}{detail}',
                    'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                    //'viewButtonImageUrl'=>Yii::app()->baseUrl .'images/dialogview.png',
                    
                       'buttons'=>array(
                                'view' => array(
                                    'label'=> 'Ver detalle',
                                ),
                                'update' => array(
                                    'label'=> 'Editar',
                                 ),
                                'delete' => array(
                                   'label'=> 'Eliminar',
                                ),
                                'detail' => array(
                                    'label' => 'Detalle',
                                    'imageUrl'=>Yii::app()->baseUrl .'/images/detail.png',
                                    'url'=>'Yii::app()->createUrl("flowdocto/child", array("id"=>$data->primaryKey))',
                                )
                                
                   ),
                   'viewDialog'=>array(
                       'controllerRoute' => 'view', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Detalle - Flujo de Trabajo',
                       'hideTitleBar' => true, 
                       'dialogWidth' => 600, //use the value from the dialog config
                       'dialogHeight' => 300,
                       'closeButtonText' => 'Cerrar'
                   ),

               //the attributes for the EFrameJuiDlg widget. use like the 'attributes' param from EQuickDlgs::iframeButton
                   'updateDialog'=>array(
                       'controllerRoute' => 'update', //=default
                       'actionParams' => array('id' => '$data->primaryKey'), //=default
                       'dialogTitle' => 'Modificar - Flujo de Trabajo',
                       'dialogWidth' => 600, //override the value from the dialog config
                       'dialogHeight' => 375,
                       'closeButtonText' => 'Cerrar'
                   ), 
               ),
	),
)); ?>

<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'create',
        'dialogTitle' => 'Agregar - Flujo de Trabajo',
        'dialogWidth' => 600,
        'dialogHeight' => 375,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'flowdocto-grid' //the grid with this id will be refreshed after closing
    )
);

?>