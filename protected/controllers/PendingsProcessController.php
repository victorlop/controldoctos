<?php

class PendingsProcessController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'PendingsProcess'),
		));
	}

	public function actionCreate() {
		$model = new PendingsProcess;


		if (isset($_POST['PendingsProcess'])) {
			$model->setAttributes($_POST['PendingsProcess']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'PendingsProcess');


		if (isset($_POST['PendingsProcess'])) {
			$model->setAttributes($_POST['PendingsProcess']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'PendingsProcess')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('PendingsProcess');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new PendingsProcess('search');
		$model->unsetAttributes();

		if (isset($_GET['PendingsProcess']))
			$model->setAttributes($_GET['PendingsProcess']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}