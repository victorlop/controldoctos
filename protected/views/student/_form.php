<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'student-form',
	'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
                <?php echo $form->hiddenField($model,'status'); ?>
                <div class="row">
		<?php echo $form->labelEx($model,'studentbatchid'); ?>
		<?php echo $form->textField($model, 'studentbatchid', array('maxlength' => 50,'size'=>30)); ?>
		<?php echo $form->error($model,'studentbatchid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'studentname'); ?>
		<?php echo $form->textField($model, 'studentname', array('maxlength' => 256,'size'=>50)); ?>
		<?php echo $form->error($model,'studentname'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 1024,'size'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div><!-- row -->                
		<div class="row">
		<?php echo $form->labelEx($model,'careerid'); ?>
                <?php echo $form->dropDownList($model, 'careerid', GxHtml::listDataEx(Career::model()->findAllAttributes(null,true))); ?>
		<?php echo $form->error($model,'careerid'); ?>
		</div><!-- row -->

<?php
echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->