<?php

class AppparameterController extends GxController {


	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Status')));	
        }

	public function actionCreate() {
		$model = new Appparameter;
                $this->performAjaxValidation($model);

		if (isset($_POST['Appparameter'])) {
			$model->setAttributes($_POST['Appparameter']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Appparameter');
                $this->performAjaxValidation($model);

		if (isset($_POST['Appparameter'])) {
			$model->setAttributes($_POST['Appparameter']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));	
	}

	public function actionDelete($id) {
            $this->loadModel($id, 'Status')->delete();

            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Appparameter('search');
		$model->unsetAttributes();

		if (isset($_GET['Appparameter']))
			$model->setAttributes($_GET['Appparameter']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Appparameter('search');
		$model->unsetAttributes();

		if (isset($_GET['Appparameter']))
			$model->setAttributes($_GET['Appparameter']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}