<?php

class CareerController extends GxController {


	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Career')));
	}

	public function actionCreate() {
		$model = new Career;

                $this->performAjaxValidation($model);
		if (isset($_POST['Career'])) {
			$model->setAttributes($_POST['Career']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Career');
                $this->performAjaxValidation($model);

		if (isset($_POST['Career'])) {
			$model->setAttributes($_POST['Career']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            
            $cuantos = Student::model()->count('status=1 and careerid=:careerid',array(':careerid'=>$id));
            if ($cuantos > 0) {
                throw new CHttpException(404, 'Carrera tiene estuciantes asignados.');
            }

            $this->loadModel($id, 'Career')->delete();

            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Career('search');
		$model->unsetAttributes();

		if (isset($_GET['Career']))
			$model->setAttributes($_GET['Career']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Career('search');
		$model->unsetAttributes();

		if (isset($_GET['Career']))
			$model->setAttributes($_GET['Career']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}