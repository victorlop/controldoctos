<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 28)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'studentid'); ?>
		<?php echo $form->textField($model, 'studentid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'studentname'); ?>
		<?php echo $form->textField($model, 'studentname', array('maxlength' => 256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'studentbatchid'); ?>
		<?php echo $form->textField($model, 'studentbatchid', array('maxlength' => 50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'entrydate'); ?>
		<?php echo $form->textField($model, 'entrydate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'statusid'); ?>
		<?php echo $form->textField($model, 'statusid', array('maxlength' => 20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'statusdescription'); ?>
		<?php echo $form->textField($model, 'statusdescription', array('maxlength' => 256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'flowdoctoid'); ?>
		<?php echo $form->textField($model, 'flowdoctoid', array('maxlength' => 20)); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
