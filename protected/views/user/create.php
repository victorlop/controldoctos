<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Create'),
);

?>


<?php

$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Agregar'=> $this->renderPartial('_form', array(	'model' => $model,),true)),
    'options'=>array(
        'collapsible'=>false,
        'active'=>0,
    ),
    'htmlOptions'=>array(
        'style'=>'width:100%;'
    ),
));


?>