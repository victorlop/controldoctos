<?php

class RolsystemoptionController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Rolsystemoption'),
		));
	}

	public function actionCreate($id) {
		$model = new Rolsystemoption;
                $model->rolid = $id;    

                $this->performAjaxValidation($model);
		if (isset($_POST['Rolsystemoption'])) {
			$model->setAttributes($_POST['Rolsystemoption']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Rolsystemoption');


		if (isset($_POST['Rolsystemoption'])) {
			$model->setAttributes($_POST['Rolsystemoption']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Rolsystemoption')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Rolsystemoption');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Rolsystemoption('search');
		$model->unsetAttributes();

		if (isset($_GET['Rolsystemoption']))
			$model->setAttributes($_GET['Rolsystemoption']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
       
}