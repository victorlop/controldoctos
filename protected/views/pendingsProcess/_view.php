<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('studentid')); ?>:
	<?php echo GxHtml::encode($data->studentid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentname')); ?>:
	<?php echo GxHtml::encode($data->studentname); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('studentbatchid')); ?>:
	<?php echo GxHtml::encode($data->studentbatchid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('entrydate')); ?>:
	<?php echo GxHtml::encode($data->entrydate); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('statusid')); ?>:
	<?php echo GxHtml::encode($data->statusid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('statusdescription')); ?>:
	<?php echo GxHtml::encode($data->statusdescription); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('flowdoctoid')); ?>:
	<?php echo GxHtml::encode($data->flowdoctoid); ?>
	<br />
	*/ ?>

</div>