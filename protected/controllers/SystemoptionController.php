<?php

class SystemoptionController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Systemoption'),
		));
	}

	public function actionCreate() {
		$model = new Systemoption;


		if (isset($_POST['Systemoption'])) {
			$model->setAttributes($_POST['Systemoption']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Systemoption');


		if (isset($_POST['Systemoption'])) {
			$model->setAttributes($_POST['Systemoption']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Systemoption')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Systemoption');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Systemoption('search');
		$model->unsetAttributes();

		if (isset($_GET['Systemoption']))
			$model->setAttributes($_GET['Systemoption']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}