<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'projectdetail-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'shortdescription'); ?>
		<?php echo $form->textField($model, 'shortdescription', array('maxlength' => 1024)); ?>
		<?php echo $form->error($model,'shortdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'longdescription'); ?>
		<?php echo $form->textArea($model, 'longdescription'); ?>
		<?php echo $form->error($model,'longdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'estimateddate'); ?>
		<?php echo $form->textField($model, 'estimateddate'); ?>
		<?php echo $form->error($model,'estimateddate'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'projectid'); ?>
		<?php echo $form->dropDownList($model, 'projectid', GxHtml::listDataEx(Project::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'projectid'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->