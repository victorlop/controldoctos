<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'project-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'projectdescription'); ?>
		<?php echo $form->textField($model, 'projectdescription', array('maxlength' => 1024)); ?>
		<?php echo $form->error($model,'projectdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments'); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dateentry'); ?>
		<?php echo $form->textField($model, 'dateentry'); ?>
		<?php echo $form->error($model,'dateentry'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'studentId'); ?>
		<?php echo $form->textField($model, 'studentId', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'studentId'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'statusId'); ?>
		<?php echo $form->textField($model, 'statusId', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'statusId'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'preprojectid'); ?>
		<?php echo $form->textField($model, 'preprojectid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'preprojectid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model, 'active'); ?>
		<?php echo $form->error($model,'active'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'flowdoctoid'); ?>
		<?php echo $form->textField($model, 'flowdoctoid', array('maxlength' => 20)); ?>
		<?php echo $form->error($model,'flowdoctoid'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('projectdetails')); ?></label>
		<?php echo $form->checkBoxList($model, 'projectdetails', GxHtml::encodeEx(GxHtml::listDataEx(Projectdetail::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('projectreviews')); ?></label>
		<?php echo $form->checkBoxList($model, 'projectreviews', GxHtml::encodeEx(GxHtml::listDataEx(Projectreview::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->