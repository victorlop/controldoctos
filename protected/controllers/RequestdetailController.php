<?php

class RequestdetailController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Requestdetail'),
		));
	}

	public function actionCreate() {
		$model = new Requestdetail;


		if (isset($_POST['Requestdetail'])) {
			$model->setAttributes($_POST['Requestdetail']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Requestdetail');


		if (isset($_POST['Requestdetail'])) {
			$model->setAttributes($_POST['Requestdetail']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Requestdetail')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Requestdetail');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Requestdetail('search');
		$model->unsetAttributes();

		if (isset($_GET['Requestdetail']))
			$model->setAttributes($_GET['Requestdetail']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}