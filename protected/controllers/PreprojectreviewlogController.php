<?php

class PreprojectreviewlogController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Preprojectreviewlog'),
		));
	}

	public function actionCreate() {
		$model = new Preprojectreviewlog;


		if (isset($_POST['Preprojectreviewlog'])) {
			$model->setAttributes($_POST['Preprojectreviewlog']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Preprojectreviewlog');


		if (isset($_POST['Preprojectreviewlog'])) {
			$model->setAttributes($_POST['Preprojectreviewlog']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Preprojectreviewlog')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Preprojectreviewlog');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Preprojectreviewlog('search');
		$model->unsetAttributes();

		if (isset($_GET['Preprojectreviewlog']))
			$model->setAttributes($_GET['Preprojectreviewlog']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}