<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'flowdocto-detail-form',
	'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model);*/ ?>
                <?php echo $form->hiddenField($model,'flowid'); ?>
                <?php echo $form->hiddenField($model,'sort'); ?>
		<div class="row">
		<?php echo $form->labelEx($model,'currentstatusid'); ?>
		<?php echo $form->dropDownList($model, 'currentstatusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)),array('empty'=>'(seleccionar)')); ?>
		<?php echo $form->error($model,'currentstatusid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'newstatusid'); ?>
                <?php echo $form->dropDownList($model, 'newstatusid', GxHtml::listDataEx(Status::model()->findAllAttributes(null, true)),array('empty'=>'(seleccionar)')); ?>
		<?php echo $form->error($model,'newstatusid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'notificationlistid'); ?>
		<?php echo $form->dropDownList($model, 'notificationlistid', GxHtml::listDataEx(Notificationlist::model()->findAllAttributes(null, true)),array('empty'=>'(seleccionar)')); ?>
		<?php echo $form->error($model,'notificationlistid'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->