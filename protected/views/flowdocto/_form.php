<div class="wide form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'flowdocto-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array('validateOnSubmit'=>true,)
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Campos con '); ?> <span class="required">*</span> <?php echo Yii::t('app', 'son requeridos.'); ?>.
	</p>

	<?php /*echo $form->errorSummary($model); */?>
                <?php echo $form->hiddenField($model,'status'); ?>
		<div class="row">
		<?php echo $form->labelEx($model,'flowdescription'); ?>
		<?php echo $form->textField($model, 'flowdescription', array('maxlength' => 256,'size'=>50)); ?>
		<?php echo $form->error($model,'flowdescription'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments',
                                            array('cols'=>50, 'rows'=>5)); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->

		<label><?php /*echo GxHtml::encode($model->getRelationLabel('flowdoctoDetails'));*/ ?></label>
		<?php /*echo $form->checkBoxList($model, 'flowdoctoDetails', GxHtml::encodeEx(GxHtml::listDataEx(FlowdoctoDetail::model()->findAllAttributes(null, true)), false, true));*/ ?>

<?php
echo GxHtml::submitButton(Yii::t('app', $model->isNewRecord ? 'Crear' : 'Grabar'));
$this->endWidget();
?>
</div><!-- form -->