<?php

/**
 * This is the model base class for the table "status".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Status".
 *
 * Columns in table "status" available as properties of the model,
 * followed by relations of table "status" available as properties of the model.
 *
 * @property string $id
 * @property string $statusdescription
 * @property integer $sort
 * @property integer $amountreviews
 * @property integer $endstatus
 * @property integer $maxdays
 * @property integer $allowinactivation
 *
 * @property FlowdoctoDetail[] $flowdoctoDetails
 * @property Preproject[] $preprojects
 * @property Preprojectreview[] $preprojectreviews
 * @property Project[] $projects
 * @property Projectreview[] $projectreviews
 * @property Request[] $requests
 * @property Rolstatus[] $rolstatuses
 */
abstract class BaseStatus extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'status';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Status|Statuses', $n);
	}

	public static function representingColumn() {
		return 'statusdescription';
	}

	public function rules() {
		return array(
			array('statusdescription, sort, amountreviews, endstatus', 'required'),
			array('sort, amountreviews, endstatus, maxdays, allowinactivation', 'numerical', 'integerOnly'=>true),
			array('statusdescription', 'length', 'max'=>256),
			array('maxdays, allowinactivation', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, statusdescription, sort, amountreviews, endstatus, maxdays, allowinactivation', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'flowdoctoDetails' => array(self::HAS_MANY, 'FlowdoctoDetail', 'currentstatusid'),
			'preprojects' => array(self::HAS_MANY, 'Preproject', 'statusid'),
			'preprojectreviews' => array(self::HAS_MANY, 'Preprojectreview', 'statusid'),
			'projects' => array(self::HAS_MANY, 'Project', 'statusId'),
			'projectreviews' => array(self::HAS_MANY, 'Projectreview', 'statusid'),
                        'requests' => array(self::HAS_MANY, 'Request', 'statusid'),
			'rolstatuses' => array(self::HAS_MANY, 'Rolstatus', 'statusid'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'statusdescription' => Yii::t('app', 'Descripción'),
			'sort' => Yii::t('app', 'Orden'),
			'amountreviews' => Yii::t('app', 'Revisores'),
			'endstatus' => Yii::t('app', 'Es Final'),
			'maxdays' => Yii::t('app', 'Max. días'),
                        'allowinactivation' => Yii::t('app', 'Permite Inactivaciones'),
			'flowdoctoDetails' => null,
			'preprojects' => null,
			'preprojectreviews' => null,
			'projects' => null,
			'projectreviews' => null,
                        'requests' => null,
			'rolstatuses' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('statusdescription', $this->statusdescription, true);
		$criteria->compare('amountreviews', $this->amountreviews);
		$criteria->compare('endstatus', $this->endstatus);
		$criteria->compare('maxdays', $this->maxdays);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>array('pageSize'=>Yii::app()->user->GetState('pageSize')),
		));
	}
}