    <h1><?php echo Yii::t('app', 'Detalle Flujo de Trabajo'); ?></h1>

        <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                    array('label' => 'Flujo de Trabajo', 
                        'value'=> $model->flowdescription
                        ,)
                ),
        )); ?>
        <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'child-grid',
                        'dataProvider'=>$child_model->searchByParent($parentId),
                        'columns'=>array(
                            //array('name'=>'id','htmlOptions'=>array('width'=>'60'),),
                            array(
                                'name'=>'Estado Actual',
                                'value'=>'GxHtml::valueEx($data->currentstatus)',
                                'filter'=>GxHtml::listDataEx(Status::model()->findAllAttributes(null, true,'', array('prompt' => Yii::t('app', 'Todos')))),
                            ),
                             array(
                                'name'=>'Nuevo Estado',
                                'value'=> function($data,$row) {
                                            return Status::model()->findByPk($data->newstatusid)->statusdescription;                                                   
                                } ,
                            ),
                            array(
                                'header' => '',
                                'name' => 'sort',
                                'htmlOptions'=>array('width'=>'20'),
                                'class' => 'ext.OrderColumn.OrderColumn',
                              ),
                           
                            array(
                                    'class'=>'EJuiDlgsColumn',
                                     'template'=>'{delete}',
                                     'htmlOptions'=>array('width'=>'10','style' => 'text-align: center;'),
                                        'buttons'=>array(
                                                 'delete' => array(
                                                    'label'=> 'Eliminar',
                                                     'url'=>'Yii::app()->createUrl("flowdoctoDetail/delete", array("id"=>$data->primaryKey))',
                                                 ),

                                    ),
                                ),
                            ),
                    ));
            
        ?>
<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'flowdoctoDetail/create/'.$parentId,
        'dialogTitle' => 'Agregar ',
        'dialogWidth' => 600,
        'dialogHeight' => 400,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'child-grid' //the grid with this id will be refreshed after closing
    )
);
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('flowdocto/admin'))); ?>
<!-- http://www.yiiframework.com/forum/index.php/topic/32985-extension-quickdlgs/ -->