<?php

class ModuleController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Module'),
		));
	}

	public function actionCreate() {
		$model = new Module;


		if (isset($_POST['Module'])) {
			$model->setAttributes($_POST['Module']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Module');


		if (isset($_POST['Module'])) {
			$model->setAttributes($_POST['Module']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Module')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Module');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Module('search');
		$model->unsetAttributes();

		if (isset($_GET['Module']))
			$model->setAttributes($_GET['Module']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        public function actionCat() {
            $this->render('cat');
        }

        public function actionConf() {
            $this->render('conf');
        }

        public function actionSeg() {
            $this->render('seg');
        }

        public function actionDoctos() {
            $this->render('doctos');
        }
}