<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'request',
			'type' => 'raw',
			'value' => $model->request !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->request)), array('request/view', 'id' => GxActiveRecord::extractPkValue($model->request, true))) : null,
			),
array(
			'name' => 'topic',
			'type' => 'raw',
			'value' => $model->topic !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->topic)), array('topic/view', 'id' => GxActiveRecord::extractPkValue($model->topic, true))) : null,
			),
'approved',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('preprojects')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->preprojects as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('preproject/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>