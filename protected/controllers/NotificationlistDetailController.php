<?php

class NotificationlistDetailController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'NotificationlistDetail'),
		));
	}

	public function actionCreate() {
		$model = new NotificationlistDetail;


		if (isset($_POST['NotificationlistDetail'])) {
			$model->setAttributes($_POST['NotificationlistDetail']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'NotificationlistDetail');


		if (isset($_POST['NotificationlistDetail'])) {
			$model->setAttributes($_POST['NotificationlistDetail']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'NotificationlistDetail')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('NotificationlistDetail');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new NotificationlistDetail('search');
		$model->unsetAttributes();

		if (isset($_GET['NotificationlistDetail']))
			$model->setAttributes($_GET['NotificationlistDetail']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}