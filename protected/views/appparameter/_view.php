<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('code')); ?>:
	<?php echo GxHtml::encode($data->code); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('parametervalue')); ?>:
	<?php echo GxHtml::encode($data->parametervalue); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('parameterdescription')); ?>:
	<?php echo GxHtml::encode($data->parameterdescription); ?>
	<br />

</div>