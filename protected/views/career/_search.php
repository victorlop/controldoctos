<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

    	<div class="row">
		<?php echo $form->label($model, 'code'); ?>
		<?php echo $form->textField($model, 'code', array('maxlength' => 10,'size'=>20)); ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model, 'careerdescription'); ?>
		<?php echo $form->textField($model, 'careerdescription', array('maxlength' => 256,'size'=>50)); ?>
	</div>
    
	<div class="row">
		<?php echo $form->label($model, 'comments'); ?>
		<?php echo $form->textArea($model, 'comments',
                                            array('cols'=>50, 'rows'=>5)); ?>
	</div>

        <div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Buscar')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
