<?php

class TeacherController extends GxController {


	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Teacher')));
	}

	public function actionCreate() {
		$model = new Teacher;
                $this->performAjaxValidation($model);

		if (isset($_POST['Teacher'])) {
			$model->setAttributes($_POST['Teacher']);
                        $model->status=1;
			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Teacher');
                $this->performAjaxValidation($model);

		if (isset($_POST['Teacher'])) {
			$model->setAttributes($_POST['Teacher']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));
	}

	public function actionDelete($id) {
            $row=  $this->loadModel($id,'Teacher');
            $row->status=0;
            $row->save();
            
            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Teacher('search');
		$model->unsetAttributes();

		if (isset($_GET['Teacher']))
			$model->setAttributes($_GET['Teacher']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Teacher('search');
		$model->unsetAttributes();

		if (isset($_GET['Teacher']))
			$model->setAttributes($_GET['Teacher']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}