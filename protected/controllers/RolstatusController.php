<?php

class RolstatusController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Rolstatus'),
		));
	}

	public function actionCreate($id) {
		$model = new Rolstatus;
                $model->rolid = $id;    

                $this->performAjaxValidation($model);
		if (isset($_POST['Rolstatus'])) {
			$model->setAttributes($_POST['Rolstatus']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Rolstatus');


		if (isset($_POST['Rolstatus'])) {
			$model->setAttributes($_POST['Rolstatus']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Rolstatus')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Rolstatus');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Rolstatus('search');
		$model->unsetAttributes();

		if (isset($_GET['Rolstatus']))
			$model->setAttributes($_GET['Rolstatus']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}