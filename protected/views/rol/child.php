    <h1><?php echo Yii::t('app', 'Permisos por rol'); ?></h1>

        <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'attributes' => array(
                    array('label' => 'Rol', 
                        'value'=> $model->roldescription
                        ,)
                ),
        )); ?>
        <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'child-grid',
                        'dataProvider'=>$child_model->searchByParent($parentId),
                        'columns'=>array(
                            //array('name'=>'id','htmlOptions'=>array('width'=>'60'),),
                            array(
                                'name'=>'Opcion',
                                'value'=>'GxHtml::valueEx($data->systemoption)',
                                'filter'=>GxHtml::listDataEx(Systemoption::model()->findAllAttributes(null, true)),
                            ),
                            array(
                                    'class'=>'EJuiDlgsColumn',
                                     'template'=>'{delete}',
                                     'htmlOptions'=>array('width'=>'80','style' => 'text-align: center;'),
                                        'buttons'=>array(
                                                 'delete' => array(
                                                    'label'=> 'Eliminar',
                                                     'url'=>'Yii::app()->createUrl("rolsystemoption/delete", array("id"=>$data->primaryKey))',
                                                 ),

                                    ),
                                ),
                            ),
                    ));
            
        ?>
<?php 

EQuickDlgs::iframeButton(
    array(
        'controllerRoute' => 'rolsystemoption/create/'.$parentId,
        'dialogTitle' => 'Agregar ',
        'dialogWidth' => 600,
        'dialogHeight' => 400,
        'openButtonText' => 'Agregar',
        'closeButtonText' => 'Cerrar',
        'closeOnAction' => true, //important to invoke the close action in the actionCreate
        'refreshGridId' => 'child-grid' //the grid with this id will be refreshed after closing
    )
);
?>
<?php    echo GxHtml::button('Regresar', array('submit'=>array('rol/admin'))); ?>
<!-- http://www.yiiframework.com/forum/index.php/topic/32985-extension-quickdlgs/ -->