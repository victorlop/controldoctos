<?php

class StatusController extends GxController {


	public function actionView($id) {
		EQuickDlgs::render('view',array('model'=>$this->loadModel($id,'Status')));
	}

	public function actionCreate() {
		$model = new Status;
                
                $this->performAjaxValidation($model);

		if (isset($_POST['Status'])) {
			$model->setAttributes($_POST['Status']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin'));
			}
		}
                else {
                    $model->maxdays=NULL;
                }

		EQuickDlgs::render('create',array('model'=>$model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Status');
                $this->performAjaxValidation($model);

		if (isset($_POST['Status'])) {
			$model->setAttributes($_POST['Status']);

			if ($model->save()) {
                            EQuickDlgs::checkDialogJsScript();
                            $this->redirect(array('admin','id'=>$model->id));
			}
		}

		EQuickDlgs::render('update',array('model'=>$model));	
        }

	public function actionDelete($id) {
            $this->loadModel($id, 'Status')->delete();

            if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionIndex() {
		$model = new Status('search');
		$model->unsetAttributes();

		if (isset($_GET['Status']))
			$model->setAttributes($_GET['Status']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function actionAdmin() {
		$model = new Status('search');
		$model->unsetAttributes();

		if (isset($_GET['Status']))
			$model->setAttributes($_GET['Status']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}